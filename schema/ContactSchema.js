const joi = require('joi');

const ContactSchema = joi.object({
    name: joi.string().required(),
    lastname: joi.string().required(),
    email: joi.string().email({ tlds: { allow: false } }).required(),
    address: joi.string().allow(null, ''),
    phoneNumber: joi.string().allow(null, '')
})

export default ContactSchema