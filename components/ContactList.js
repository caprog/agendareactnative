import React, { useState } from 'react';
import { Text, View, Button, StyleSheet, TouchableHighlight } from 'react-native';
import Styles from '../Styles';
import Constants from './Constants';
import ContactService from './ContactService';

function ContactList({ navigation }) {
    const [data, setData] = useState({
        loaded: false,
        contacts: []
    })
    
    if(!data.loaded) {
        const updateContacts = function (contacts) {
            setData({loaded: true, contacts})
        }
        ContactService.subscribeContacts(updateContacts)
        ContactService.findContacts().then(updateContacts)
    }

    const removeContact = (id) => ContactService.removeContact(id).then(r => {})
    const edit = contactEmail => {
        console.log(contactEmail);
        navigation.navigate('UpdateContact', { email : contactEmail })
    }

    const Item = function ({id, name}) {
        return (
            <TouchableHighlight onPress={ () => edit(id) } >
                <View style={ContactStyle.item}>
                    <View style={ContactStyle.item_btn}>
                        <Button key={'btn' + id} 
                            title="Remove" 
                            color={ Constants.red }  
                            onPress={() => removeContact(id)}/>
                    </View>
                    <Text key={'label' + id} style={ Styles.foreground_carbon }> { name + ' - ' + id }</Text>
                </View>
            </TouchableHighlight>
        )
    }
    const ContactList = ({contacts}) => contacts.map(c => <Item key={'item' + c.id} name={c.name} id ={c.id}/>)
    
    return (
        <View style={ Styles.container }>
            <ContactList contacts= { data.contacts }/>
        </View>
    )
}

export default ContactList

const ContactStyle = StyleSheet.create({
    item: {
        display: 'flex', 
        flexDirection: 'row', 
        alignItems: 'center',
        marginTop: '6px',
        marginBottom: '6px',
        backgroundColor: '#fff',
        borderRadius: 3,
        padding: '12px'
    },

    item_btn: {
        marginRight: '15px',
    }
})