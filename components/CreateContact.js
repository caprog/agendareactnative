import React from 'react';
import { Alert, Button, TextInput, View } from 'react-native';
import { useState } from 'react';
import Styles from '../Styles';
import '@firebase/firestore';

import ContactService from './ContactService';
import ContactSchema from '../schema/ContactSchema';
import CustomAlert from './CustomAlert';
import Constants from './Constants';

function CreateContact({navigation}) {
    const initialState = {
        name: '',
        lastname: '',
        email: '',
        phoneNumber: '',
        address: ''
    }
    const [contact, setContact] = useState({...initialState})

    const [error, setError] = useState('')

    const submit = () => {
        const {error, value} = ContactSchema.validate(contact)
        
        if(error){
            setError(error.message)
        } else
            ContactService.saveContact(contact)
                .then(r => {
                    setContact({...initialState})
                    setError('')
                    navigation.navigate('Contacts')
                })
    }

    const handleField = (name, value) => setContact({...contact, [name] : value })
    
    const renderTextInput = (placeholder, name) => <TextInput style={Styles.input_txt} value={ contact[name] } placeholder={placeholder} onChangeText={txt => handleField(name, txt)} />

    return (
        <View style={Styles.container}>

            { renderTextInput('Name *', 'name') }
            { renderTextInput('Lastname *', 'lastname') }
            { renderTextInput('Email *', 'email') }
            { renderTextInput('Phone number', 'phoneNumber') }
            { renderTextInput('Address', 'address') }
            
            <CustomAlert msg={ error } backgroundColor={ Constants.red } color={ Constants.white } show={ error !== '' } onPress={ () => setError('') }/>
            
            <View style={Styles.margin_top}>
                <Button title="Create" onPress={submit} />
            </View>
        </View>
    )
}

export default CreateContact