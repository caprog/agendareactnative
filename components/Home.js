
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import ContactList from './ContactList';
import UpdateContact from './UpdateContact';

const Stack = createStackNavigator();

function Home() {
    return (
        <Stack.Navigator initialRouteName="Contacts">
            <Stack.Screen name="Contacts" component={ContactList} />
            <Stack.Screen name="UpdateContact" component={UpdateContact} options={ { title: 'Update contact' }}/>
        </Stack.Navigator>
    )
}

export default Home