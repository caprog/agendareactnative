
import firebase from 'firebase/app';
import '@firebase/firestore';

const observers = []
const ContactService = {
    subscribeContacts: function (observer) {
        observers.push(observer)
    },

    notifyAll: function () {
        this.findContacts()
            .then(r => {
                observers.forEach(obs => obs(r))
            })
    },

    findContacts: function () {
        return new Promise((res, rej) => { 
            firebase.firestore()
                .collection('contacts')
                .get()
                .then(r => res(r.docs.map(d => ({ id: d.id, ...d.data()}))))
                .catch(e => rej(e))
        })
    },

    findContact: function (email) {
        return new Promise((res, rej) => { 
            firebase.firestore()
                .collection('contacts')
                .doc(email)
                .get()
                .then(r => res(r.data()))
                .catch(e => rej(e))
        })
    },

    saveContact: function (contact) {
        return new Promise((res, rej) => {
            firebase.firestore()
                .collection('contacts')
                .doc(contact.email)
                .set({
                    ...contact
                })
                .then(r => {
                    res(r)
                    this.notifyAll()
                })
                .catch(e => rej(e))
        })
    },

    updateContact: function (contact) {
        return new Promise((res, rej) => {
            firebase.firestore()
                .collection('contacts')
                .doc(contact.email)
                .update({
                    ...contact
                })
                .then(r => {
                    res(r)
                    this.notifyAll()
                })
                .catch(e => rej(e))
        })
    },

    removeContact: function (contactId) {
        return new Promise((res, rej) => {
            firebase.firestore()
                .collection('contacts')
                .doc(contactId)
                .delete()
                .then(r => {
                    res(true)
                    this.notifyAll()
                })
                .catch(e => rej(e))
        })
    }
}
export default ContactService