import React from 'react';
import { Text, TouchableHighlight, View } from 'react-native';

function CustomAlert({ msg, backgroundColor, color, show, onPress }) {
    const style = { ...CustomAlertStyle.container, backgroundColor, color } 
    
    if(!show)
        style.display = 'none'

    return ( 
        <TouchableHighlight onPress={ onPress } >
            <View style={ style } onPress= { onPress }>
                <Text style={ { color } }>{ msg }</Text>
                <Text style={ { color, marginRight: '6px' } }>x</Text>
            </View>
        </TouchableHighlight>
    )
}

const CustomAlertStyle = {
    container: {
        display: 'flex',
        flexDirection: 'row', 
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: '6px',
        marginBottom: '6px',
        borderRadius: 3,
        padding: '12px'
    }
}

export default CustomAlert