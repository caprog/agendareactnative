
import React from 'react';
import { Button, TextInput, View } from 'react-native';
import { useState } from 'react';
import Styles from '../Styles';
import '@firebase/firestore';

import ContactService from './ContactService';
import ContactSchema from '../schema/ContactSchema';
import CustomAlert from './CustomAlert';
import Constants from './Constants';

function UpdateContact({ navigation, route }) {
    const initialState = {
        name: '',
        lastname: '',
        email: '',
        phoneNumber: '',
        address: ''
    }

    const [loaded, setLoaded] = useState(false)
    const [contact, setContact] = useState({...initialState})
    const [error, setError] = useState('')

    if(route.params && route.params.email && !loaded)
        ContactService.findContact(route.params.email)
            .then(contact => {
                setLoaded(true)
                setContact(contact)
                console.log(contact);
            })
            .catch(e => console.log(e))

    const submit = () => {
        const {error, value} = ContactSchema.validate(contact)
        
        if(error)
            setError(error.message)
        else
            ContactService.updateContact(contact)
                .then(r => {
                    setContact({...initialState})
                    setError('')
                    navigation.navigate('Contacts')
                })
    }

    const handleField = (name, value) => setContact({...contact, [name] : value })
    
    const renderTextInput = (placeholder, name) => {
        return (
            <TextInput style={Styles.input_txt} 
                value={ contact[name] } 
                placeholder={placeholder} 
                onChangeText={txt => handleField(name, txt)} 
            />
        )
    }

    return (
        <View style={Styles.container}>
            
            <TextInput 
                style={Styles.input_txt_disabled} 
                value={ contact.email } 
                placeholder="Email" 
                editable={ false }
            />

            { renderTextInput('Name *', 'name') }
            { renderTextInput('Lastname *', 'lastname') }
            { renderTextInput('Phone number', 'phoneNumber') }
            { renderTextInput('Address', 'address') }
            
            <CustomAlert msg={ error } backgroundColor={ Constants.red } color={ Constants.white } show={ error !== '' } onPress={ () => setError('') }/>
            
            <View style={Styles.margin_top}>
                <Button title="Update" onPress={submit} />
            </View>
        </View>
    )
}

export default UpdateContact