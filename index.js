import 'react-native-gesture-handler';
import { registerRootComponent } from 'expo';
import firebase from 'firebase/app';

import App from './App';


const firebaseConfig = {
    apiKey: "AIzaSyD-CZXbcw5Me92DuzKALUx5v14SfJ99i0Y",
    authDomain: "agendareactnative-31227.firebaseapp.com",
    projectId: "agendareactnative-31227",
    storageBucket: "agendareactnative-31227.appspot.com",
    messagingSenderId: "661015162152",
    appId: "1:661015162152:web:128513b3bb268d0db7741d"
  };
  
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

// registerRootComponent calls AppRegistry.registerComponent('main', () => App);
// It also ensures that whether you load the app in Expo Go or in a native build,
// the environment is set up appropriately
registerRootComponent(App);
