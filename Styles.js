'use strict';
import { StyleSheet } from 'react-native';

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#202124',
        padding: 15
    },

    foreground_white: {
        color: 'white'
    },

    foreground_carbon: {
        color: '#191919'
    },

    input_txt: {
        backgroundColor: 'white',
        padding:12,
        marginTop: 6,
        marginBottom: 6,
        borderRadius: 3
    },

    input_txt_disabled: {
        backgroundColor: '#BBBBBB',
        padding:12,
        marginTop: 6,
        marginBottom: 6,
        borderRadius: 3
    },

    margin_top: {
        marginTop: 12
    }
});

export default Styles